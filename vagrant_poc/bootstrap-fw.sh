#!/usr/bin/env bash
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y install iptables-persistent

if [[ -f "/vagrant/rules-fw-misc.v4" ]]; then
    cp /vagrant/rules-fw.v4 /etc/iptables/rules.v4
	iptables-restore /etc/iptables/rules.v4
	sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
	echo 1 | tee /proc/sys/net/ipv4/ip_forward
fi

if [[ -f "/vagrant/rc.local" ]]; then
    cp /vagrant/rc.local /etc/rc.local
fi
