CREATE DATABASE IF NOT EXISTS hotel;
USE hotel;
CREATE TABLE rooms (
id INT AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30) NOT NULL,
email VARCHAR(50),
roomtype text,
startdate DATE,
enddate   DATE
);
