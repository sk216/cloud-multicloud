<!DOCTYPE html>
<html>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
th, td {
  padding: 5px;
  text-align: left;    
}
</style>

<head>
	<title>SQL Injection</title>
	<link rel="shortcut icon" href="../Resources/hmbct.png" />
</head>
<body>

 <div style="background-color:#c9c9c9;padding:15px;">
      <button type="button" name="homeButton" onclick="location.href='../index.html';">Home Page</button>
	</div>

	<div   align="center">
	<form action="<?php $_SERVER['PHP_SELF']; ?>" method="post" >
		<h1>Search your booking</h1>
		<label> name </label> <input type="text" name="name">
                 <br>  or </br>
		<label> email </label> 
	       <input type="text" name="email"><br/>
  	       <input type="submit" name="submit" value="Submit">
	</form>
	</div>


<?php
$envfile='../.env';
if(file_exists($envfile)){
	require_once '../init.php';
}
$servername = getenv('DB_HOST');
$username = getenv('DB_USER');
$password = getenv('DB_PASS');
$db = getenv('DB_NAME');
	// Create connection
	$conn = mysqli_connect($servername,$username,$password,$db);
	// Check connection
	if (!$conn) {
    	die("Connection failed: " . mysqli_connect_error());
	} 
	//echo "Connected successfully";
	if(isset($_POST["submit"])){
		$name = $_POST["name"];
		$email = $_POST["email"];
		$sql = "SELECT *  FROM rooms WHERE name='$name' OR email='$email'";//String
		$result = mysqli_query($conn,$sql);
		if (mysqli_num_rows($result) > 0) {
			$rows  = array(); 
			echo '</br></br>';
			echo '<table class="data-table" align="center">
				<tr class="data-heading">';  
			while ($property = mysqli_fetch_field($result)) {
				echo '<td>' . $property->name . '</td>'; 
				array_push($rows, $property->name);  
			}
			echo '</tr>'; 

			while ($row = mysqli_fetch_array($result)) {
				echo "<tr>";
				foreach ($rows as $item) {
					echo '<td>' . $row[$item] . '</td>'; //get items using property value
				}
				echo '</tr>';
			}
			echo "</table>";
		} else {
			echo "0 results";
		}
	}

?>
</body>
</html>
