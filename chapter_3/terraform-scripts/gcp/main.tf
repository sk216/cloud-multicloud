provider "google" {
  project = var.project_name
  region  = var.project_region
  zone    = var.project_region_zone
 credentials = file(var.gcp_credentials_path)
}

resource "google_compute_firewall" "default" {
  name    = "allow"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80","22"]
  }
}

resource "google_compute_instance" "web" {
  name         = "web-frontend"
  machine_type = "f1-micro"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
 }

network_interface {
    network       = "default"
    access_config {
    }
 }
metadata = {
   ssh-keys = "demo:${file(var.public_key_path)}" 
 }
provisioner "file" {
  source      = var.install_script_src_path
  destination = "/tmp/install.sh"

  connection {
    host     = self.network_interface.0.access_config.0.nat_ip
    type     = "ssh"
    user     = "demo"
    private_key = file(var.private_key_path)
  }
}
provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/install.sh",
      "sudo /tmp/install.sh ",
    ]
 connection {
    host     = self.network_interface.0.access_config.0.nat_ip
    type     = "ssh"
    user     = "demo"
    private_key = file(var.private_key_path)
  }
}
}
 

 





