variable "project_region" {
  default = "us-east1"
}

variable "project_region_zone" {
  default = "us-east1-b"
}

variable "project_name" {
  description ="provide your google project name"
  default = "cloud-multicloud"
}

variable "gcp_credentials_path" {
  description = "provide your google credential path"
}

variable "public_key_path" {
  description = "provide your ssh public key path"
  #default
}

variable "private_key_path" {
  description = "provide your ssh public key path"
  #default
}

variable "install_script_src_path" {
  description = "Path to install script within this repository"
  default     = "install.sh"
}




