variable "key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.
Example: ~/.ssh/terraform.pub
DESCRIPTION
#default="your public key"
}

variable "key_name" {
  description = "Desired name of AWS key pair"
 # default="your keyname"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-east-1"
}

variable "install_path" {
  description = "install file path"
  default     = "./install.sh"
}

