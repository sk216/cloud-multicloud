provider "aws" {
  region = var.aws_region
}

resource "aws_security_group" "default" {
  name        = "allow ssh_http"
  ingress {
    from_port   = 22 
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   }

  ingress {
    from_port   = 80 
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
   }
 egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
 tags= {
    Name = "Demo-WebApp-rules"
  }

}

resource "aws_key_pair" "auth" {
  key_name   = var.key_name
  public_key = file(var.key_path)
}

data "aws_ami" "ubuntu" {
    most_recent = true

    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
    }

    owners = ["099720109477"]  # owner
}

resource "aws_instance" "web" {
  instance_type = "t2.micro"
  ami = data.aws_ami.ubuntu.id
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.default.id]
  key_name = aws_key_pair.auth.key_name
  user_data = file(var.install_path)
  tags = {
    Name = "Demo-frontend"
  }
}
