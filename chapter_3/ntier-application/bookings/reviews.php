<!DOCTYPE html>
<html>
  <head>
    <title>reviews</title>
  </head>
  <body>
    <link rel="stylesheet" href="../Resources/button.css"/>
    <link rel="stylesheet" href="../Resources/table.css"/>
    <link rel="stylesheet" href="../Resources/materialize.min.css"/>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
 <nav>
    <div class="nav-wrapper grey lighten-1">
      <a href="#" class="brand-logo">Welcome</a>
	 <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li><a href="../index.html">Home</a></li>
      </ul>
    </div>
  </nav>

  <div class="form-style-8">
	    <h2>Post your review</h2>
	    <form action="postreview.php" method="post">
		    <textarea   name="message-area"   placeholder="message"  onkeyup="adjust_textarea(this)"></textarea>
		    <input type="text" name="name" placeholder="Your Name" />
		    <input type="submit" value="Post"/>
	    </form>
    </div>
    <ul class="collection">
	      <li class="collection-item avatar">
			<i class="material-icons circle blue">play_arrow</i>		
		          <img src="images/yuna.jpg" alt="" class="circle">
			      <span class="title">Demo</span>
			          <p>I stayed in the hotel on christmas vacation
				         very good experience 
				             </p>
				          <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
			         </li>
   </ul>

<ul class="collection">
<?php
  $docroot = $_SERVER["DOCUMENT_ROOT"];
  include($docroot. '/bookings/db_conn.php');
  $conn = db_connect();
  $sql = "select *  from reviews";
  $result = mysqli_query($conn,$sql);
  while ($row = mysqli_fetch_array($result)) {
	  echo '<li class="collection-item avatar">';
	  echo '<i class="material-icons circle red">play_arrow</i>';
          echo '<img src="images/yuna.jpg" alt="" class="circle">';
          echo '<span class="title">'.  $row[3]. '&nbsp;&nbsp;&nbsp; <b>'. $row[2] . ' </b></span>';
          echo ' <p>' . $row[1] .  '</p>';
          echo ' <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>';
	  echo  ' </li>';
  }


?> 
</ul>
 </body>
</html>
