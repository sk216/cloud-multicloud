<?php

function db_connect(){
	$envfile='../.env';
	if(file_exists($envfile)){
        	require_once '../init.php';
	}
	$servername = getenv('DB_HOST');
	$username = getenv('DB_USER');
	$password = getenv('DB_PASS');
	$db = getenv('DB_NAME');
	$conn = mysqli_connect($servername,$username,$password);
	if (!$conn) {
	      die("Connection failed: " . mysqli_connect_error());
	}
	if (!$conn->select_db($db)) {
	        echo "Database doesn't exits";
	}
	return $conn;
}
?>
