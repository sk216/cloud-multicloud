## Deploying standalone multi-tier application  
Install apache, php and mysql services 

    sudo apt-get update
    sudo apt-get install apache2
    sudo apt-get install mysql-server
    sudo apt-get install php libapache2-mod-php php-mysql
   
clone the app from repo

    git clone https://gitlab.com/sk216/cloud-multicloud.git

either  copy the application code to /var/www/html dir  or create a soft link to the app folder. 

    cp -R ntier-application/*  /var/www/html
	          or 
    ln -s ~/cloud-multicloud/chapter_3/ntier-application/   /var/www/html
verify the app

    curl localhost/info.php

