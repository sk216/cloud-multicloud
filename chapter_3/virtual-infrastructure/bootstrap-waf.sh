#!/usr/bin/env bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

apt-get update

DEBIAN_FRONTEND=noninteractive apt-get -y install docker-ce
usermod -aG docker vagrant
# Assuming the git clone of cloud-multicloud is done in vagrant, has to be changed
cd /vagrant; docker build -t hotel-waf -f /vagrant/Dockerfile .

#after starting vagrant up waf, "docker run -p 80:80 -it --rm hotel-waf
#DEBIAN_FRONTEND=noninteractive apt-get -y install libapache2-mod-security2
#DEBIAN_FRONTEND=noninteractive apt-get -y install git

#if [[ -f "/vagrant/security2.conf" ]]; then
  #cp -rf /vagrant/crs-setup.conf /etc/modsecurity/crs-setup.conf
  #cp -rf /vagrant/rules /etc/modsecurity/
  #cp -rf /vagrant/security2.conf /etc/apache2/mods-available/security2.conf
#fi
